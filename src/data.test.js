import data from './data'

test('data matches snapshot', () => {
	expect(data).toMatchSnapshot()
	expect(data.length).toBeGreaterThan(0)
})
