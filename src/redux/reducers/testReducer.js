const initialState = {}

const testReducer = (state = initialState, action) => {
	switch (action) {
		default:
			return state
	}
}

export { testReducer }
