import React, { useState } from 'react'
import { Button, Card } from 'antd'

function App() {
	const [counter, setCounter] = useState(0)
	return (
		<div className="root">
			<h1>Hi</h1>
			<Button type="primary" className="btn" onClick={() => setCounter(counter + 1)}>
				Click
			</Button>
			<Button
				type="outlined"
				className="dec"
				disabled={counter <= 0 ? true : false}
				onClick={() => counter > 0 && setCounter(counter - 1)}
			>
				Clicks
			</Button>
			<span className="counter">{counter}</span>
		</div>
	)
}

export default App
