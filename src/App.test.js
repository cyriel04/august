import { shallow } from 'enzyme'
import React from 'react'
import App from './App'

describe('Before testing App', () => {
	let wrapper
	beforeEach(() => {
		wrapper = shallow(<App />)
	})

	test('h1 text should be correct', () => {
		expect(wrapper.find('h1').text()).toContain('Hi')
	})

	test('initial state should be correct', () => {
		expect(wrapper.find('.counter').text()).toBe('0')
	})

	test('increment should increment', () => {
		wrapper.find('.btn').simulate('click')
		expect(wrapper.find('.counter').text()).toBe('1')
	})

	test('decrement should decrement', () => {
		wrapper.find('.btn').simulate('click')
		expect(wrapper.find('.counter').text()).toBe('1')

		wrapper.find('.dec').simulate('click')
		expect(wrapper.find('.counter').text()).toBe('0')
	})

	test('decrement should not go beyond 0', () => {
		wrapper.find('.dec').simulate('click')
		expect(wrapper.find('.counter').text()).not.toBe('-1')
	})
})
